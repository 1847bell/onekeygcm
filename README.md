## OneKeyGcm

Install [Mojo-Webqq](https://github.com/sjdy521/Mojo-Webqq) And [Mojo-Weixin](https://github.com/sjdy521/Mojo-Weixin) With Push Plugin.

### Usage

Execute the following command as ROOT:

> We recommend that you use CentOS 7 or Debian 8 to avoid strange problems.

- Install **curl** and **screen** tools

```
# RedHat / CentOS
yum -y install curl screen

# Debian / Ubuntu
apt-get update
apt-get -y install curl screen
```

- Create a window in UTF-8 mode

```
screen -U -S yourname
```
> <http://www.cnblogs.com/mchina/archive/2013/01/30/2880680.html>

> linux screen 命令详解

- Download and run this script

```
curl -LkO https://gitlab.com/auroreme/onekeygcm/raw/master/OneKeyGcm.sh

bash OneKeyGcm.sh
```
After running the script, you only need to select installation module, push type and enter the registration id can be automatically deployed to complete.

### Security

Due to the default configuration security is extremely low, please refer to the [security_config_demo.pl](security_config_demo.pl) and the following tutorial to security reinforcement:

**Please don't share unsafe server address on anywhere. If appear security risks, the consequence is proud!!!!!!**

<https://milkice.me/2017/03/gcmformojo-anquanxiangguanjiaocheng-security_tutorial/>

> GcmForMojo 安全相关系列 - Milkice's Blog
