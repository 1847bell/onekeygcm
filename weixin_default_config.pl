﻿#!/usr/bin/env perl

use Mojo::Weixin;

my $client = Mojo::Weixin->new(
    log_encoding => 'utf8'
);

$client->load('UploadQRcode');

$client->load('ShowMsg');

$client->load(
    'GCM',
    data => {
        api_url => 'https://gcm-http.googleapis.com/gcm/send',
        api_key => 'AIzaSyB18io0hduB_3uHxKD3XaebPCecug27ht8',
        registration_ids => ['xxx']
    }
);

$client->load(
    'Openwx',
    data => {
        listen => [
            {
                host => '0.0.0.0',
                port => 3000
            }
        ]
    }
);

$client->run();
